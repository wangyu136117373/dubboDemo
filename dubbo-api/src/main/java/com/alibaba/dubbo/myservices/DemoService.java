package com.alibaba.dubbo.myservices;

import java.util.List;

/**
 * Created by HOREN on 2018/8/28.
 */
public interface DemoService {
    List<String> getPermission(Long id);
}
