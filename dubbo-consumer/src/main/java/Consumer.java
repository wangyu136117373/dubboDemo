import com.alibaba.dubbo.myservices.DemoService;
import com.alibaba.dubbo.myservices.DemoServiceV2;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by HOREN on 2018/8/28.
 */
public class Consumer {
    public static void main(String[] args) {
        //测试常规服务
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("consumer.xml");
        context.start();
        System.out.println("consumer start");
        DemoService demoService = context.getBean(DemoService.class);
        DemoServiceV2 demoServiceV2=context.getBean(DemoServiceV2.class);
        System.out.println(demoServiceV2.sayHello());
    }
}
