package com.alibaba.dubbo.providers;

import com.alibaba.dubbo.myservices.DemoService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HOREN on 2018/8/28.
 */
public class DemoServiceImpl implements DemoService {

    public List<String> getPermission(Long id) {
        List<String> demo = new ArrayList<String>();
        demo.add(String.format("Permission_%d", id - 1));
        demo.add(String.format("Permission_%d", id));
        demo.add(String.format("Permission_%d", id + 1));
        return demo;
    }
}
